﻿public static class Random
{
    private static readonly System.Random RandomGenerator = new System.Random();

    /// <summary>
    /// Returns random value inside specified range.
    /// </summary>
    public static float Range(float min, float max)
        => (float) RandomGenerator.NextDouble() * (max - min) + min;

}