﻿using UnityEngine;

/// <summary>
/// Class demonstrating steering behavior usage.
/// </summary>
public class WanderBehaviour : MonoBehaviour
{
    [SerializeField] private float speed = 1;
    [SerializeField] private float rotation = 1;
    [SerializeField] private float radius = 1;
    [SerializeField] private float offset = 1;
    [SerializeField] private float jitter = 1;

    private WanderSteering steering;

    private void Awake()
        => steering = new WanderSteering(radius, offset, jitter);

    private void Update()
    {
        // Calculating current steering.
        var steeringDirection = steering.Update();
        
        // Steering uses pure C# System.Numeric.Vector2; transforming into UnityEngine.Vector2.
        var worldDirection = new Vector2(steeringDirection.X, steeringDirection.Y);
        
        // Transforming to object space.
        Vector3 localDirection = transform.rotation * new Vector3(worldDirection.x, 0, worldDirection.y);
        
        // Applying steering to position and rotation.
        // Using deltaTime and Lerp to achieve smooth movements.
        transform.position += speed * Time.deltaTime * localDirection;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(localDirection, Vector3.up), rotation * Time.deltaTime);
        
        // Visualize.
        DebugDraw(localDirection);
    }
    
    // Visualization:    
    private void DebugDraw(Vector3 direction)
    {
        // Draw current direction.
        Debug.DrawRay(transform.position, direction, Color.red);

        // Draw circle.
        const int segments = 30;
        Vector3 center = transform.position + transform.rotation * Vector3.forward * offset;
        Vector3 point = center + Vector3.forward * radius;
        for (var i = 1; i <= segments; i++)
        {
            Vector3 next = center + Quaternion.Euler(0, 360 / segments * i, 0) * Vector3.forward * radius;
            Debug.DrawLine(point, next);
            point = next;
        }
    }
}