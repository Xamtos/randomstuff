﻿using System.Numerics;

/// <summary>
/// Flexible steering for random movements.
/// </summary>
internal class WanderSteering : ISteering
{
    private readonly float circleRadius;
    private readonly float circleOffset;
    private readonly float targetJitter;

    private Vector2 target;

    public WanderSteering(float circleRadius, float circleOffset, float targetJitter)
    {
        this.circleRadius = circleRadius;
        this.circleOffset = circleOffset;
        this.targetJitter = targetJitter;
    }

    public Vector2 Update()
    {
        // Translate current target to random direction.
        target += new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * targetJitter;
        
        // Place target onto the circle.
        target = Vector2.Normalize(target) * circleRadius;

        // Offset the circle and return the result.
        return target + new Vector2(0, circleOffset);
    }
}