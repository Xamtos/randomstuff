﻿using System.Numerics;

public interface ISteering
{
    Vector2 Update();
}