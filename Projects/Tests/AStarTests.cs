using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using AI.A_Star;
using NUnit.Framework;

namespace Tests
{
    public class AStarTests
    {
        private const int MaxSteps = 100005;
        private readonly IPath reusablePath = new Path(MaxSteps);

        private static object[] TestData
            => new object[]
            {
                new TestCaseData(Array.Empty<Vector2Int>(), new Vector2Int(-5, -5), new Vector2Int(5, 5), 11),
                new TestCaseData(new[] {new Vector2Int(-1, 3), new Vector2Int(0, 3), new Vector2Int(1, 3), new Vector2Int(2, 3)}, new Vector2Int(0, 0),
                    new Vector2Int(0, 5), 6),
                new TestCaseData(new[]
                    {
                        new Vector2Int(1, 1), new Vector2Int(1, 2), 
                        new Vector2Int(1, 3), new Vector2Int(1, 4), 
                        new Vector2Int(1, 4), new Vector2Int(1, 4), 
                        new Vector2Int(1, 5), new Vector2Int(1, 6),
                        new Vector2Int(1, 7), new Vector2Int(1, 8),
                        new Vector2Int(1, 9), new Vector2Int(1, 10),
                        new Vector2Int(0, 10),
                        new Vector2Int(-1, 10), new Vector2Int(-2, 10),
                        new Vector2Int(-3, 10)
                    }, new Vector2Int(0, 0),
                    new Vector2Int(0, 11), 14)
            };

        [TestCaseSource(nameof(TestData))]
        public void RandomTest(Vector2Int[] obstacles, Vector2Int start, Vector2Int end, int steps)
        {
            IPath path = new Path(MaxSteps);

            path.Calculate(start, end, obstacles, out var result);

            Console.WriteLine(string.Join(", ", result));
            Assert.AreEqual(steps, result.Count);
        }

        [Test]
        public void PerformanceTestSinglePass()
        {
            const int steps = 2000;
            const int loops = 1000;
            List<float> results = new List<float>();
            Vector2Int start = new Vector2Int(-steps, -steps);
            Vector2Int end = new Vector2Int(steps, steps);

            for (int i = 0; i < loops; i++)
            {
                var path = new Path(MaxSteps);
                var sw = Stopwatch.StartNew();
                path.Calculate(start, end, Array.Empty<Vector2Int>(), out var result);
                results.Add(sw.ElapsedMilliseconds);
            }
            
            Console.WriteLine(results.Skip(1).Average());
        }

        [Test]
        public void PerformanceTestMultiplePasses()
        {
            const int steps = 1000;
            const int loops = 1000;
            List<float> results = new List<float>();
            Vector2Int start = new Vector2Int(-steps, -steps);
            Vector2Int end = new Vector2Int(steps, steps);

            for (int i = 0; i < loops; i++)
            {
                var sw = Stopwatch.StartNew();
                reusablePath.Calculate(start, end, Array.Empty<Vector2Int>(), out var result);
                results.Add(sw.ElapsedMilliseconds);
            }
            
            Console.WriteLine(results.Skip(1).Average());
        }
    }
}